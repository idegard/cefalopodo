package com.idegard.LanANN;

import java.util.HashMap;

import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.util.TransferFunctionType;

public class LanguageANN {
	
	public enum Idiomas{
		EN,ES,FR;
		double[] getOutput(){
			switch(this){
			case EN:
				return new double[] { 1,0,0 };
			case ES:
				return new double[] { 0,1,0 };
			case FR:
				return new double[] { 0,0,1 };
			}
			return null;
		}
		String getName(){
			switch(this){
			case EN:
				return "Ingles";
			case ES:
				return "Espanol";
			case FR:
				return "Frances";
			}
			return null;
		}
	}
	
	
	MultiLayerPerceptron myMlPerceptron;
	
	public LanguageANN(){
		DataSet trainingSet = new DataSet(26, 3); 

		trainingSet.addRow(new DataSetRow(LanguageInput.generateFromFile("en1.txt"), Idiomas.EN.getOutput()));
		trainingSet.addRow(new DataSetRow(LanguageInput.generateFromFile("en2.txt"), Idiomas.EN.getOutput()));
		trainingSet.addRow(new DataSetRow(LanguageInput.generateFromFile("en3.txt"), Idiomas.EN.getOutput()));
		trainingSet.addRow(new DataSetRow(LanguageInput.generateFromFile("es1.txt"), Idiomas.ES.getOutput()));
		trainingSet.addRow(new DataSetRow(LanguageInput.generateFromFile("es2.txt"), Idiomas.ES.getOutput()));
		trainingSet.addRow(new DataSetRow(LanguageInput.generateFromFile("es3.txt"), Idiomas.ES.getOutput()));
		trainingSet.addRow(new DataSetRow(LanguageInput.generateFromFile("fr1.txt"), Idiomas.FR.getOutput()));
		trainingSet.addRow(new DataSetRow(LanguageInput.generateFromFile("fr2.txt"), Idiomas.FR.getOutput()));
		trainingSet.addRow(new DataSetRow(LanguageInput.generateFromFile("fr3.txt"), Idiomas.FR.getOutput()));

		myMlPerceptron = new MultiLayerPerceptron(TransferFunctionType.TANH, 26, 2, 3);

		myMlPerceptron.learn(trainingSet);
	}
	
	public double[] evaluateLanguage(String string){
		myMlPerceptron.setInput(LanguageInput.generateFromString(string));
		myMlPerceptron.calculate();
		return myMlPerceptron.getOutput();
	}
	
	public String getLanguageName(String string){
		double[] result= evaluateLanguage(string);
		HashMap<String, Double> mapaResult = new HashMap<String, Double>();
		mapaResult.put(Idiomas.EN.getName(), result[0]);
		mapaResult.put(Idiomas.ES.getName(), result[1]);
		mapaResult.put(Idiomas.FR.getName(), result[2]);
		return MapUtils.sortByComparator(mapaResult).entrySet().iterator().next().getKey();
		
	}

	public static void main(String args[]){
		LanguageANN a = new LanguageANN();
		String prueba1= "Como los eruditos que lo precedieron, el doctor Lecter almacena un asombroso c�mulo de datos asociados a objetos de estas mil estancias; "
				+ "pero, a diferencia de los antiguos, su palacio cumple una segunda funci�n: a temporadas le sirve de residencia. Ha pasado a�os rodeado por sus exquisitas ";
		String prueba2="Authorization Server that can be used to kickstart your API authentication. In essence it enables you to focus on your actual resource"
				+ " endpoints and use the out-of-the-box authorization server to authenticate resource owners and subsequently validate the access tokens that were granted to the "
				+ "Client applications.";
		String prueba3="Une l�gende gasconne affirme qu�il y avait autrefois, dans la Montagne (les Pyr�n�es), un Serpent long de cent toises, plus gros que les "
				+ "troncs des vieux ch�nes, avec des yeux rouges, et une langue en forme de grande �p�e. Ce Serpent comprenait et parlait les langues de tous les pays ";
		System.out.println(a.getLanguageName(prueba1));
		System.out.println(a.getLanguageName(prueba2));
		System.out.println(a.getLanguageName(prueba3));
	}
	
}
