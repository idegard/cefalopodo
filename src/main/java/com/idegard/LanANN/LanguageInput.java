package com.idegard.LanANN;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.StringUtils;

public class LanguageInput {

	private static char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
	private static TreeMap<Character, Double> mapa = new TreeMap<Character, Double>();

	public static double[] generateFromFile(String file) {
		resetMapa();
		InputStream fstream = Thread.currentThread().getContextClassLoader().getResourceAsStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		int totalCharacters = 0;
		try {
			while ((strLine = br.readLine()) != null) {
				char[] result = StringUtils.lowerCase(strLine).toCharArray();
				for (char token : result) {
					if (Character.isAlphabetic(token)) {
						if (mapa.containsKey(token)) {
							mapa.put(token, mapa.get(token) + 1);
							totalCharacters++;
						}
					}
				}
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getArrayFromMap(mapa, totalCharacters);
	}

	public static double[] generateFromString(String string) {
		resetMapa();
		int totalCharacters = 0;
		char[] result = StringUtils.lowerCase(string).toCharArray();
		for (char token : result) {
			if (Character.isAlphabetic(token)) {
				if (mapa.containsKey(token)) {
					mapa.put(token, mapa.get(token) + 1);
					totalCharacters++;
				}
			}
		}
		return getArrayFromMap(mapa, totalCharacters);
	}

	private static void resetMapa() {
		mapa.clear();
		for (char d : alphabet)
			mapa.put(d, 0d);
	}

	private static double[] getArrayFromMap(Map mapa, int totalCharacters) {
		double resultado[] = new double[mapa.size()];
		int indice = 0;
		for (Object a : mapa.values().toArray())
			resultado[indice++] = ((Double) a).doubleValue() / totalCharacters;
		return resultado;
	}

	public static void main(String[] args) {
		for (double a : generateFromFile("en1.txt"))
			System.out.println(a);
	}

}
