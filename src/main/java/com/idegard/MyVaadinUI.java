package com.idegard;

import javax.servlet.annotation.WebServlet;

import com.idegard.LanANN.LanguageANN;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@Theme("mytheme")
@SuppressWarnings("serial")
public class MyVaadinUI extends UI
{
	LanguageANN neurona = new LanguageANN();
	
    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = MyVaadinUI.class, widgetset = "com.idegard.AppWidgetSet")
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        setContent(layout);
        final TextArea area = new TextArea("Escribe algo, presiona el boton Analizar y la super sistema determinará mediante un ANN el idioma del texto...");
        area.setSizeFull();
        final Label label = new Label();
        label.setSizeUndefined();
        label.addStyleName(ValoTheme.LABEL_H1);
        label.addStyleName(ValoTheme.LABEL_BOLD);
        Button button = new Button("Analizar");
        button.addClickListener(new Button.ClickListener() {
            public void buttonClick(ClickEvent event) {
            	label.setValue(neurona.getLanguageName(area.getValue()));
            }
        });
        layout.setSpacing(true);
        layout.addComponent(area);
        layout.addComponent(button);
        layout.addComponent(label);
        layout.setComponentAlignment(area, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(button, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
    }

}
