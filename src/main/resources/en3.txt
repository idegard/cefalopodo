Should I look him in the eye when I killed him or just shoot him in the back?

Tough call.

I crouched in the kitchen, digging through my purse as if searching for keys. I knew right where the gun was, of course, but pulling it straight out just seemed so � obscene. The smell of dinner on the stove filled my nose. Chicken chili, with whole-grain corn bread in the oven for a side because it�s healthier.

It�d been baking for ten minutes already, which meant I had about twelve more minutes to end his life before the bread burned.

Reese sat out in the dining room, reading one of his motorcycle magazines and drinking his favorite beer while he waited for food. I�d been sure to buy a half rack earlier, and I�d met him at the door with a cold one open and ready to go. He was on his second now. I wasn�t under any illusions�two beers wouldn�t be enough to slow him down if he came after me, or ease his pain if my aim was off.

Still, a man deserves a beer before dying, right?

My fingers brushed the cold metal of the gun. I pulled out my phone instead and looked at Jessica�s picture, studying her pretty, smiling face on graduation day. So full of hope and promise. She�d raised her right arm to wave at the camera. Her pinkie curled forward, offering a glimpse of the sparkling tips on her new acrylics. She�d wanted them for graduation so badly. They hadn�t been in the budget, but I couldn�t tell her no.

You have to understand�none of us ever expected Jessica to graduate.

Hell, she shouldn�t even be alive. My bitch of a cousin had done drugs all through both pregnancies, yet somehow Jessie pulled through. Not unscathed. She had the usual developmental quirks � poor impulse control, bad judgment. Quick to anger. They came from fetal drug effects�the gift that keeps on giving for a child�s whole life. But at least she had a life. Her little sister died in the NICU two days after her birth. Never got a chance.

Fuck you, Amber. Fuck you very much for doing that to your kids.

I glanced up at the oven timer and realized I�d wasted nearly three minutes thinking about Jess. I supposed I could kill him after pulling out the bread, but putting it off would just make things harder.

Or maybe I should feed him first?

No. He�d had his beer, but if I had to sit across from Reese over a meal I�d never make it. I couldn�t look into those blue eyes and laugh. I�d never been a good liar. This past month had been heaven and hell rolled into one big bad joke.

Right. Time for the punch line.

I pulled out the small pistol and stuck it into the pocket of the loose sweater I�d picked so carefully for just this moment. I also took out my keys, my ID, and my cash, stuffing them into my jeans. Just in case. I didn�t really expect to survive the night, but it never hurts to hope. The van was even gassed up and ready to go, on the off chance that I managed to get away.

Of course, I had no idea where I�d drive. Burn that bridge when you get to it �

Things started going wrong as soon as I walked into the dining room. Reese wasn�t sitting at the head of the table, where I�d left him. Damn. I could�ve shot him in the back without warning if he�d just stayed put. Now he sat facing me, leaning casually in his chair, beer in hand. The magazine lay open before him and he looked up, offering me that mocking smile of his. God, I loved that smile, despite the fact that it could be cruel as all f**k.

�Something you want to talk about?� he asked, co**cking his head.

�No,� I murmured, wondering what he�d say if I shared my thoughts. Gee, Reese, I�m so sorry I�m about to kill you, but if it makes you feel any better I hate myself for doing it�not a hundred percent sure I won�t shoot myself next.

I wouldn�t, though. Not yet. Not until I saw Jessica for myself, made sure they�d kept their promises and she was safe and sound. After that?

Well. We�d just have to see.

He sighed, eyes flicking to my pocket, where my hand shifted nervously on the gun.

Paranoia hit yet again.

He knew. He knew all about it, I could see it in his face. Fuck. I�d failed her � Don�t be ridiculous. How could he possibly know?

�Babe, you look like you could use a day off,� he said finally. �Have you considered hitting the spa? Maybe get a massage?�

�That costs too much,� I said automatically, biting back a hysterical laugh. Because money mattered now, right?

�I wasn�t suggesting that you pay for it,� he said, frowning at me.

�I don�t want your money��

�Yeah, I know, you�re totally independent and you like it that way. Blah, blah. Just let me do something for you, for once. Fuck�s sake.�

Shit. Why did he have to be so nice?

I felt my eyes start to water and I looked away, forcing myself to detach again and focus. I needed to kill him, and I couldn�t give him any warning. But he was facing me and all the way across the room, which was a bigger problem than it sounds. Pistols aren�t exactly known for accuracy, and it�s not like I had much in the way of experience.

I needed to get closer.

If I came up behind him, rubbed his shoulders � That would be close enough. God, I was a shitty human being.

�The food won�t be ready for another ten minutes,� I said. �You look sort of tense. Want a neck rub?�

He raised a brow as I circled the table.

�I think you should stay back,� he said slowly. I paused.

�What do you mean?�

�Well, I�d hate to make it too easy for you, sweetheart.�

My chest tightened. I offered a weak smile, because like I said�I�m a shit liar.

�I don�t understand.�

�I�m as**suming you�re planning to shoot me in the back of the head,� he said quietly, and that�s when I realized he wasn�t relaxed at all. He might be leaning back casually, but every one of those solid muscles roping his body had drawn tight, poised to attack. �That�s a bad idea. You shoot that close, you�ll be all covered in blood spatter. Means you�ll have to risk tracking more evidence out of the house or taking time to clean up. Either way, complicates things.�

Well. At least it was all finally out in the open. Almost a relief. I pulled out the gun and held it up, using my left hand to brace my right as I carefully sighted on him. I expected him to explode up at me, to fight back. Instead he just sat, waiting.

�Go ahead, do it,� he said, a sad smile toying with the corners of his mouth. �Show me what you�re made of.�

�I�m so sorry,� I whispered. �You�ll never know how much I wish this weren�t happening.�

�Then don�t do it. Whatever it is, we can work through it. I�ll help you.�

�You can�t.�

He sighed, then looked past me and jerked his chin.

�It�s over, babe,� I heard a man say from behind. Huh. I guess it was. Fortunately, I had just enough time to pull the trigger before he hit me.





CHAPTER ONE


EIGHTEEN DAYS EARLIER

LONDON

My back was killing me.

It was nearly two in the morning, and I�d just finished up the late-night cleaning shift at the pawn shop. I�d been letting myself get soft the past couple of months. Too much time spent managing the business, not enough time scouring bathrooms, because I�d forgotten just how much work scrubbing a toilet really is.