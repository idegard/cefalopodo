There was one universal truth in Lucky Harbor, Washington�you could hide a pot of gold in broad daylight and no one would steal it, but you couldn�t hide a secret.

There�d been a lot of secrets in Aubrey Wellington�s colorful life, and nearly all of them had been uncovered and gleefully discussed ad nauseam.

And yet here she was, still in this small Pacific West Coast town she�d grown up in. She didn�t quite know what that said about her other than that she was stubborn as hell.

In any case, she was fairly used to bad days by the time she walked to Lucky Harbor�s only bar and grill, but today had taken the cake. Ted Marshall, ex�town clerk, ex-boss, and also, embarrassingly enough, her ex-lover, was self-publishing his own tell-all. And since he�d ever so thoughtfully given her an advance reading copy, she knew he was planning on informing the entire world that, among other things, she was a bitchy, money-hungry man-eater.

She�d give him the money-hungry part. She was sinking much of her savings into her aunt�s bookstore, the Book & Bean, a sentimental attempt at bringing back the one happy childhood memory she had. The effort was leaving her far too close to broke for comfort. She�d even give him the bitchy part�at least on certain days of the month.

But man-eater? Just because she didn�t believe in happily-ever-afters, or even a happily-for-now, didn�t mean she was a man-eater. She simply didn�t see the need to invite a man all the way into her life when he wouldn�t be staying.

Because they never stayed.

She shrugged off the little voice that said That�s your own fault and entered the Love Shack. Stepping inside the bar and grill was like going back a hundred years into an old western saloon. The walls were a deep, sinful bordello red and lined with old mining tools. The ceiling was covered with exposed beams, and lanterns hung over the scarred bench-style tables, now filled with the late dinner crowd. The air hummed with busy chatter, loud laughter, and music blaring out of the jukebox against the far wall.

Aubrey headed straight for the bar. �Something that�ll make my bad day go away,� she said to the bartender.

Ford Walker smiled and reached for a tumbler. He�d been five years ahead of Aubrey in school, and was one of the nice ones. He�d gone off and achieved fame and fortune racing sailboats around the world, and yet he�d chosen to come back to Lucky Harbor to settle down.

She decided to take heart in that.

He slid her a vodka cranberry. �Satisfaction guaranteed,� he promised.

Aubrey wrapped her fingers around the glass, but before she could bring it to her lips, someone nudged her shoulder.

Ted, the ex-everything.

�Excuse me,� he began before recognition hit and the �Oh, shit� look came into his eyes. He immediately started to move away, but she grabbed his arm.

�Wait,� she said. �I need to talk to you. Did you get my messages?�

�Yeah,� he said. �All twenty-five of them.� Ted had been born with an innate charm that usually did a real good job of hiding the snake that lay beneath it. Even now, he kept his face set in an expression of easy amusement, exuding charisma like a movie star. With a wry smile for anyone watching, he leaned in close. �I didn�t know there were that many different words for as**shole.�

�And you still wouldn�t if you�d have called me back even once,� she said through her teeth. �Why are you doing this? Why did you say those things about me in your book? And in chapter one!� She�d stopped reading after that and maybe had tossed the book, with great satisfaction, into a Dumpster.

Ted shrugged and leaned back. �I need the money.�

�Am I supposed to believe anyone�s going to buy your book?�

�Hey, if the only buyers are Lucky Harbor residents, I still make five grand, baby.�

�Are you kidding me?�

�Not even a little bit,� he said. �What�s the big deal, anyway? Everyone writes a book nowadays. And besides, it�s not like you�re known for being an angel.�

Aubrey knew exactly who she was. She even knew why. She didn�t need him to tell her a damn thing about herself. �The big deal is that you�re the one who wronged people,� she said. It was a huge effort to keep her voice down. She wasn�t as good at charm and charisma as he was. �You two-timed me�along with just about every other woman in town, including the mayor�s wife! On top of that, you let her steal fifty grand of the town�s funds�and yet somehow, I�m the bad guy.�

�Hey,� he said. �You were the town clerk�s admin. If anyone should have known what had happened to that money, it was you, babe.�

How had she ever worked for this guy? How had she ever slept with him? Her friend Ali had told her that every woman had at least one notch on her bedpost she secretly regretted. But there was no secret to Aubrey�s regret. She gripped her tumbler so tight that she was surprised it didn�t shatter. �You said things about me that had nothing to do with the money.�

He smiled. �So the book needed a little�titillation.�

Shaking with fury, she stood. �You know what you are?�

�A great guy?�

Her arm bypassed her brain and capped off her no-good very bad day by tossing her vodka cranberry in his smug face.

But though he was indeed at least twenty-five kinds of an as**shole, he was also fast as a whip. He ducked, and her drink hit the man on the other side of him.

Straightening, Ted chortled in delight as Aubrey got a look at the man she�d inadvertently drenched. She stopped breathing. Oh, God. Had she really thought her day couldn�t get any worse? Why would she tempt fate by even thinking that? Because of course things had gotten worse. They always did.

Ben McDaniel slowly stood up from his bar stool, dripping vodka from his hair, eyelashes, nose�he was six-feet-plus of hard muscles and brute strength on a body that didn�t carry a single extra ounce of fat. For the past five years, he�d been in and out of a variety of Third World countries, designing and building water systems with the Army Corps of Engineers. His last venture had been for the Department of Defense in Iraq, which Aubrey only knew because Lucky Harbor�s Facebook page was good as gospel.

Ted was already at the door like a thief in the night, the weasel. But not Ben. He swiped his face with his arm, deceptively chill and laid-back.

In truth, he was about as badass as they came.

Aubrey should know; she�d seen him in action. But she managed to meet his gaze. Cool, casual, even. One had to be with Ben: The man could spot weakness a mile away. �I�m sorry,� she said.

�Are you?�

She felt herself flush. He�d always seemed to see right through her. And she was pretty sure he�d never cared for her. He had good reason for that, she reminded herself. He just didn�t know the half of it.

�Yes, I am sorry,� she said. Her heart was pounding so loudly she was surprised she could hear herself speak. �Are you okay?�

He ran his fingers through a sexy disorder of sun-streaked brown hair. His eyes were the same color�light milk chocolate marbled with gold caramel. It was difficult to make such a warmly colored gaze seem hard, but Ben managed it with no effort at all. �Need to work on your aim,� he said.